import PostComponent from './components';
import rl from './rl';

const { Container: PostContainer } = rl;

export default PostContainer(PostComponent);
