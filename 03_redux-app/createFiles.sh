mkdir -p src/modules/post;
mkdir src/modules/post/components;
mkdir src/modules/post/containers
mkdir src/modules/post/types;
mkdir src/modules/post/actions;
mkdir src/modules/post/reducer;
mkdir src/modules/post/epics;

touch src/modules/post/index.js;
touch src/modules/post/components/index.jsx;
touch src/modules/post/containers/index.js;
touch src/modules/post/types/index.js;
touch src/modules/post/actions/index.js;
touch src/modules/post/reducer/index.js;
touch src/modules/post/epics/index.js;
touch src/reducers.js; #w/ code
touch src/store.js; #w/ code
yarn add redux react-redux;
yarn add redux-logger; #redux-logger: to check current state:
yarn add prop-types; #prop-types: to validate react component properties
#testing
yarn add mocha chai sinon enzyme nyc enzyme-adapter-react-16 -D;
#test
yarn add eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react nsp jsdom redux-mock-store babel-register -D;
#production
yarn add eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react nsp jsdom redux-mock-store babel-register -D;
touch .eslintignore;
touch .eslintrc;
mkdir tests;
touch tests/helper.js;
touch .nycrc;
mkdir -p tests/modules/post/types;
touch tests/modules/post/types/index.js;
mkdir tests/modules/post/actions;
touch tests/modules/post/actions/index.js;
mkdir tests/modules/post/containers;
touch tests/modules/post/containers/index.jsx;
mkdir tests/modules/post;
touch tests/modules/post/index.jsx;
