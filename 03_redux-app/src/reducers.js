import { combineReducers } from 'redux';

import post from './modules/post/reducer';

export default combineReducers({ post });
