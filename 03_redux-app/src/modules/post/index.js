import PostComponent from './components';
import PostContainer from './containers';

export default PostContainer(PostComponent);
