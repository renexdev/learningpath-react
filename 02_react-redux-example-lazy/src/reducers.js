import { combineReducers } from 'redux';
//Creo type, actions & reducer y lo importo

import rl from './form/rl';
//prior to lazy
//import form from './form/reducer';

//empty
//export default combineReducers({});

export default combineReducers({ [rl.nameSpace]: rl.reducer });
//prior to lazy
//export default combineReducers({ form });

/*
const {
    nameSpace,
    types,
    actions,
    defaultState,
    reducer,
    mapStateToProps,
    mapDispatchToProps,
    Container,
} = rl.flush();
*/