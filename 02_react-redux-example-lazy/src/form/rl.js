import RL from 'redux-lazy';

const rl = new RL('form');
rl.addAction('text', { text: '' });
rl.addAction('submit', {});

const result = rl.flush();

export default result;


//types.js
//export const FORM_TEXT = '@@form/TEXT';
//export const FORM_SUBMIT = '@@form/SUBMIT';
//reducer.js
//import { FORM_TEXT } from './types';

//const defaultState = {
//    text: '',
//};

//export default (state = defaultState, action) => {
//    switch (action.type) {
//        case FORM_TEXT:
//            return { ...state, ...action };
 //       default:
 //           return state;
//    }
//};
//actions.js
//import { FORM_TEXT, FORM_SUBMIT } from './types';

//export const textAction = text => ({
//    type: FORM_TEXT,
//    text,
//});

//export const submitAction = () => ({
 //   type: FORM_SUBMIT,
//});