## React Article Tutorial ##
This repository can be used to follow along with my Medium tutorial on React. Link for the article is below:

[Medium Article](https://medium.com/p/86f4cf197097/)

### How To Install ###
 1. `git clone https://github.com/DZuz14/react-medium-tutorial.git`
 2.  cd into the folder via terminal
 3. Type `npm install`
 4. Done.
 
 To run the app, just type `npm start` from terminal when you are inside the root directory of the folder.
