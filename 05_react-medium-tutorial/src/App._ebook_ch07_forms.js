import React, { Component, createElement } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
//import GeneralForm from './ebook/ch07_forms/GeneralForm'
import EmailForm from './ebook/ch07_forms/EmailForm'
//import AccountForm from './ebook/che07_forms/AccountForm'


const node  = document.querySelector('.main');
//const jsxElement =  <GeneralForm data-url="http://webapplog.com" />
//const jsxElement =  <AccountForm />
const jsxElement =  <EmailForm />
render(jsxElement, node)