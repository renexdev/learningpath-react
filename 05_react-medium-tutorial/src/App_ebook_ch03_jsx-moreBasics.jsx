import React, { Component, createElement } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'

const node  = document.querySelector('.main');
var specialChars = {__html: '&copy;&mdash;&ldquo;'}
class Content extends React.Component {

  getUrl() {
    return 'http://webapplog.com'
  }
  render() {
    return (
      <div>
        <p>Using built-in function getUrl:</p>
        <p>Your REST API URL is: <a href={this.getUrl()}>{this.getUrl()}</a></p>
        <p>Special chars:</p>
        <span dangerouslySetInnerHTML={specialChars}/>
        <p>Passing data:</p>
        <li object-id="097F4E4F">li with data</li>
        <li data-object-id="097F4E4F">li without data</li>
        <p>Styling:</p>
        <span style={
          {borderColor: 'blue',
          borderWidth: 4,
          borderStyle: 'solid'}
        }>Hey</span>
        <span style={{border: '1px red solid'}}>Hey</span>
        <p>class and for:</p>
        <input type="radio" name={this.props.name} id={this.props.id}></input>
        <label htmlFor={this.props.id}>{this.props.label}</label>
        <p>Boolean attribute values:</p>
        <input disabled={false} />
        <input disabled="false" />
        <input disabled />
      </div>
    )
  }
}

const jsxElement = <Content name='pepe' id='5'/>

render(jsxElement, node)