import React from 'react';
import { render } from 'react-dom';
import Home from './components/Home'
import  Card from './components/Card'
//import  App from './components/Counter'
//import  App from './components/Color'
import  App from './components/ColorStyled'
import  CounterButtonV1 from './components/CounterButtonV1'
import  CounterButtonV2 from './components/CounterButtonV2'
import  CounterApp from './components/CounterButtonV3'
import  Input from './components/Input'
import  AppRenderingV1 from './components/ConditionalRenderingV1'
import  AppRenderingV2 from './components/ConditionalRenderingV2'
import  AppRenderingV3 from './components/ConditionalRenderingV3'
import  List from './components/List'
import  RefsV1 from './components/RefsV1'
import  AppTodo from './components/toDoList/AppTodo'
import  AppReducer from './components/Reducer'
//Redux lib
import { createStore } from 'redux'
import { reducer } from './components/toDoListRedux/reducer'
import  AppTodoRedux from './components/toDoListRedux/AppTodoRedux'
import  AppTodoReactRedux from './components/toDoListReactRedux/AppTodoReactRedux'
import { Provider } from 'react-redux'


const node  = document.querySelector('.main');

const element = <Home />


const element1 = (
  <div>
    <Card color={'skyblue'}>Card 1</Card>
    <Card color={'steelblue'}>Card 2</Card>
  </div>
)

const element2 = (
  <div>
    <input type={'text'} defaultValue={'Type here!'} />
    <select>
      <option>A</option>
      <option>B</option>
    </select>
    <img src={'//unsplash.it/200/300'} />
  </div>
)
const element3 = <App />
const element4 = <CounterButtonV1 />
const element5 = <CounterButtonV2 />
const element6 = <CounterApp />
const element7 = <Input />
const element8 = <AppRenderingV1 />
const element9 = <AppRenderingV2 />
const element10 = <AppRenderingV3 />
const element11 = <List />

const element12 = <RefsV1 />
const element13 = <AppTodo />

const element14 = <AppReducer />



// Import the reducer and create a store
const store = createStore(reducer)
element15 = <AppTodoRedux store={store} />;
const element16 = (
  <Provider store={store}>
    <AppTodoReactRedux />
  </Provider>
)
render(element16, node)