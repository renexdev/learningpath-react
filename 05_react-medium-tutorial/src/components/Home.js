import React, { Component } from 'react';
import HeadlineButton from './HeadlineButton';
class Home extends Component {
	constructor(props){
		super(props);
    this.state = {
      headline: false
    }
    this.toggleHeadline = this.toggleHeadline.bind(this);
	}
  
  //With ES2016 class instance properties, we can instead write func = () =>. func is then bound to the class instance at construction.
  //toggleHeadline = () => {
  //  this.setState({headline: !this.state.headline});
  //}
  toggleHeadline(){
    this.setState({headline: !this.state.headline});
  }

  render() {
    return (
      <div>
          <h1>{this.state.headline? "Headline is false": "Headline is true"}</h1>
          <HeadlineButton toggle={this.toggleHeadline} />
      </div>
    );
  }
}

export default Home;
