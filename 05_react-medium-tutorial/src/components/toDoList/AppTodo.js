import React, { Component } from 'react'

import List from './List'
import Input from './Input'
import Title from './Title'

export default class AppTodo extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
    todos: ['Click to remove', 'Learn React', 'Write Code', 'Ship App'],
  };
    this.onAddTodo = this.onAddTodo.bind(this);
    this.onRemoveTodo = this.onRemoveTodo.bind(this);

  }


  onAddTodo (text){
    const {todos} = this.state

    this.setState({
      todos: [text, ...todos],
    })
  }

  onRemoveTodo (index) {
    const {todos} = this.state

    this.setState({
      todos: todos.filter((todo, i) => i !== index),
    })
  }

  render() {
    const {todos} = this.state

    return (
      <div style={styles.container}>
        <Title>
          To-Do List
        </Title>
        <Input
          placeholder={'Type a todo, then hit enter!'}
          onSubmitEditing={this.onAddTodo}
        />
        <List
          list={todos}
          onClickItem={this.onRemoveTodo}
        />
      </div>
    )
  }
}

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
  }
}
