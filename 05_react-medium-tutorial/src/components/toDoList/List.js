import React, { Component } from "react";

export default class List extends Component {
  
  constructor(props) {
    super(props);
    this.renderItem = this.renderItem.bind(this);

  }
  renderItem(text, i){
    const { onClickItem } = this.props;

    return (
      <div style={styles.item} onClick={() => onClickItem(i)}>
        {text}
      </div>
    );
  };

  render() {
    const { list } = this.props;

    return (
      <div style={styles.container}>
        {list.map(this.renderItem)}
      </div>
    );
  }
}

const styles = {
  container: {
    display: "flex",
    flexDirection: "column"
  },
  item: {
    backgroundColor: "whitesmoke",
    marginBottom: 5,
    padding: 15
  }
};
