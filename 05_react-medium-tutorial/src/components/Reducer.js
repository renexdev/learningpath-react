import React, { Component } from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'

// Define action types
const types = {
  INCREMENT: 'INCREMENT',
}

// Define a reducer
/*
You then define a function to handle actions, and update
 the store accordingly. You can choose how to update the
  state depending on which type of action your reducer 
  function receives. Redux will pass this function the 
  current state of the store, and the action you 
  dispatched, expecting an updated state object to
   be returned: (state, action) => newState. We
    call this function a reducer function.
*/
const reducer = (state, action) => {
  if (action.type === types.INCREMENT) {
    return {count: state.count + 1}
  }
  return state
}

// Define the initial state of our store
const initialState = {count: 0}

// Create a store, passing our reducer function and our initial state
const store = createStore(reducer, initialState)


/// We're done! Redux is all set up. Here's how we can use it:


// Now we can dispatch actions, which are understood by our store/reducer
store.dispatch({type: types.INCREMENT})
store.dispatch({type: types.INCREMENT})
store.dispatch({type: types.INCREMENT})

// Calling `store.getState()` returns our state object
class AppReducer extends Component {
  render() {
    return (
      <div style={{fontSize: 100}}>
        {store.getState().count}
      </div>
    )
  }
}

export default AppReducer;
