import React, { Component } from 'react'
import { render } from 'react-dom'
import styled from 'styled-components'

const randomColor = () => '#' + Math.random().toString(16).substr(-6)

const Card = styled.div`
  padding: 20px;
  text-align: center;
  color: white;
  background-color: ${props => props.color};
`

const Container = styled.div`
  padding: 20px;
`

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {color: 'skyblue'};
    this.randomizeColor = this.randomizeColor.bind(this);

  }

  randomizeColor () {
    this.setState({color: randomColor()})
  }

  render() {
    const {color} = this.state
    return (
      <Container>
        <Card color={color}>
          <input
            type={'button'}
            value={'Randomize Color'}
            onClick={this.randomizeColor}
          />
        </Card>
      </Container>
    )
  }
}

export default App;
