//Version 2
import React, { Component } from 'react'
import { render } from 'react-dom'

class CounterButtonV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {count: 0};
    this.handleClick = this.handleClick.bind(this);

  }

  handleClick () {
    const {count} = this.state

    this.setState({count: count + 1})
  }

  render() {
    const {count} = this.state

    return (
      <button type='button' onClick={this.handleClick}>
        Click HERE to increment: {count}
      </button>
    )
  }
}
  export default CounterButtonV2;
