//Version 3

import React, { Component } from 'react'
import { render } from 'react-dom'

class CounterButtonV3 extends Component {
  render() {
    console.log(this.props);
    const {onPress, children} = this.props

    return (
      <button type='button' onClick={onPress}>
        {children}
      </button>
    )
  }
}

class CounterApp extends Component {

  constructor(props) {
    super(props);
    this.state = {count: 0};
    this.handlePress = this.handlePress.bind(this);

  }

  handlePress () {
    const {count} = this.state

    this.setState({count: count + 1})
  }

  render() {
    const {count} = this.state

    return (
      <CounterButtonV3
        count={count} //Solo para pasarselo
        topu={count} //Solo para pasarselo
        onPress={this.handlePress}
      >
        Click HERE to increment: {count}
      </CounterButtonV3>
    )
  }
}

export default CounterApp;
