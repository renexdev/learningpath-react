import React, { Component } from 'react'

import { actionCreators } from './reducer'
import List from '../toDoList/List'
import Input from '../toDoList/Input'
import Title from '../toDoList/Title'

export default class AppTodoRedux extends Component {

 constructor(props) {
    super(props);
    this.state = {};
    this.onAddTodo = this.onAddTodo.bind(this);
    this.onRemoveTodo = this.onRemoveTodo.bind(this);

  }


  componentWillMount() {
    const {store} = this.props

    const {todos} = store.getState()
    this.setState({todos})

    this.unsubscribe = store.subscribe(() => {
      const {todos} = store.getState()
      this.setState({todos})
    })
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  onAddTodo (text) {
    const {store} = this.props

    store.dispatch(actionCreators.add(text))
  }

  onRemoveTodo (index){
    const {store} = this.props

    store.dispatch(actionCreators.remove(index))
  }

  render() {
    const {todos} = this.state

    return (
      <div style={styles.container}>
        <Title>
          To-Do List
        </Title>
        <Input
          placeholder={'Type a todo, then hit enter!'}
          onSubmitEditing={this.onAddTodo}
        />
        <List
          list={todos}
          onClickItem={this.onRemoveTodo}
        />
      </div>
    )
  }
}

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
  }
}
