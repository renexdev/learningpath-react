//Version 1
import React, { Component } from 'react'
import { render } from 'react-dom'

class CounterButtonV1 extends Component {
  constructor(props) {
    super(props);
    this.state = {count: 0};
  }
  render() {
    const {count} = this.state;
    return (
      <button type='button' onClick={() => this.setState({count: count + 1})}>
      Click HERE to increment: {count}
      </button>
      )
    }
  }
  export default CounterButtonV1;
