import React, { Component } from 'react'
import { render } from 'react-dom'

class RefsV1 extends Component {
  constructor(props) {
    super(props);
    this.state = {width: null,height: null,};
    this.measure = this.measure.bind(this);
    this.saveRef = this.saveRef.bind(this);

  }

  saveRef (ref) {
    this.containerNode = ref;
  }

/*
We now have a reference to a DOM node, so we can get its 
width and height. We want to store these and use
 them in our render function. The easiest place
  to store these is in the component's state, so 
  we'll use setState. This triggers a second render. 
  Now we can grab the dimensions from state and display
 them.
*/
  measure() {
    const {clientWidth, clientHeight} = this.containerNode

    this.setState({
      width: clientWidth,
      height: clientHeight,
    })
  }
//We want to measure our rendered node after both 
//the initial render and subsequent renders. 
//To do this, we will measure the node in both 
//componentDidMount and componentDidUpdate.
  componentDidMount() {
    console.log('componentDidMount...');
    this.measure()
  }
/*
However, after our second render, the content will 
be different, so the node will need to be measured 
again. This means componentDidUpdate is called,
 which calls measure, which calls setState again, 
 thus causing an infinite loop.
 Preventing infinite loops with shouldComponentUpdate
*/
  componentDidUpdate() {
    console.log('componentDidUpdate...');
    this.measure()
  }

/*We can prevent the infinite loop by telling our 
component to only render if width or height has changed.
*/
  shouldComponentUpdate(nextProps, nextState) {
  console.log('shouldComponentUpdate...');

    return (
      this.state.width !== nextState.width ||
      this.state.height !== nextState.height
    )
  }

  render() {
    const {width, height} = this.state

    return (
      <div
        style={styles.card}
        ref={this.saveRef} // pass a callback function that can save the reference to the instance
      >
        <h2 style={styles.subtitle}>My dimensions are:</h2>
        {width && height && (
          <h1 style={styles.title}>{width} x {height}</h1>
        )}
      </div>
    )
  }
}

const styles = {
  card: {
    padding: 20,
    margin: 20,
    textAlign: 'center',
    color: 'white',
    backgroundColor: 'skyblue',
    border: '1px solid rgba(0,0,0,0.15)',
  },
  title: {
    fontSize: 18,
    lineHeight: '24px',
  },
  subtitle: {
    fontSize: 14,
    lineHeight: '18px',
  },
}

  export default RefsV1;