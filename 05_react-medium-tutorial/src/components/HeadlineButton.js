import React from 'react';

const HeadlineButton = (props) => {

	return <a onClick={props.toggle} className="btn btn-1g btn-info">Change me</a>
}

export default HeadlineButton;