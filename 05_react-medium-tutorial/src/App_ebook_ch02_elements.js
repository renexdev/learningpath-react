import React, { Component, createElement } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'

const node  = document.querySelector('.main');


class Hello extends Component {
  render() {
    //return <div>Hello {this.props.toWhat}</div>; //JSX code
    //return createElement('div', null, `Hello ${this.props.toWhat}`); //
    //return createElement('a',
    //  {href: 'http://webapplog.com'},
    //  'Webapplog.com'
    //  )
  }
}

//const element = <Hello toWhat="World" />
//const element =  createElement(Hello, {toWhat: 'World'}, null)
const h1 = React.createElement('h1', null, 'Hello world!')
//All the parameters after the second one become child elements
const element =  createElement('div', null, h1, h1)
render(element, node)


