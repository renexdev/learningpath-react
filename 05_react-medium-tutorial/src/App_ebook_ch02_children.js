import React, { Component, createElement } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'

const node  = document.querySelector('.main');

class HelloWorld extends React.Component {
  render() {
	//console.log("log: ",Object.isFrozen(this.props))
	console.log("log: ",this.props)
    //return createElement('div', null, h1, h1, h1, h1)
	return createElement(
      'h1', //Element
      this.props, // What I pass if I'm upper (ok, it will pass to h1) Passing all the properties from HelloWorld to <h1>
      'Hello ' + this.props.frameworkName + ' world!'
    )
  }
}
//<h1 id="ember" title="A framework for creating ambitious web applications.">Hello Ember.js world!</h1>

const element =  createElement(
    'div',
    null,
    createElement(HelloWorld, {
      id: 'ember',
      frameworkName: 'Ember.js',
      title: 'A framework for creating ambitious web applications.'}),
    createElement(HelloWorld, {
      id: 'backbone',
      frameworkName: 'Backbone.js',
      title: 'Backbone.js gives structure to web applications...'}),
    createElement(HelloWorld, {
      id: 'angular',
      frameworkName: 'Angular.js',
      title: 'Superheroic JavaScript MVW Framework'})
  )
const jsxElement =   <h1>Hello world!</h1>
render(jsxElement, node)