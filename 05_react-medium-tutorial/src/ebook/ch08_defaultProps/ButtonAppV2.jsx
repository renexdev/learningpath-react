import React, { Component, createElement } from 'react';
import ButtonV2 from "./ButtonV2"

export default class ButtonAppV2 extends Component {
  render() {
    let number = 1
    return (
      <div>
        <ButtonV2 buttonLabel="Start"/>
        <ButtonV2 />
        <ButtonV2 title={number}/>
        <ButtonV2 />
        <ButtonV2 email="not-a-valid-email"/>
        <ButtonV2 email="hi@azat.co"/>
      </div>
    )
  }
}