import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';

export default class ButtonV1 extends Component {
  render() {
    return <button className="btn" >{this.props.buttonLabel}</button>
  }
}
ButtonV1.defaultProps = {buttonLabel: 'Submit'}
//prop-types
ButtonV1.propTypes = {
  buttonLabel: PropTypes.string.isRequired
};