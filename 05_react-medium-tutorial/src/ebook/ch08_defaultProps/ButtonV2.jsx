import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';


export default class ButtonV2 extends Component {
  render() {
    return <button className="btn">{this.props.buttonLabel}</button>
  }
}

ButtonV2.defaultProps = {buttonLabel: 'Submit'}

ButtonV2.propTypes = {
  handler: PropTypes.func.isRequired,
  title: PropTypes.string,
  email(props, propName, componentName) {
    let emailRegularExpression = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    if (!emailRegularExpression.test(props[propName])) {
      return new Error('Email validation failed!')
    }
  }
}