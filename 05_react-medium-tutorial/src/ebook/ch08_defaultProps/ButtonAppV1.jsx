import React, { Component, createElement } from 'react';
import ButtonV1 from "./ButtonV1"

export default class ButtonAppV1 extends Component {
  render() {
    return (
      <div>
        <ButtonV1 buttonLabel="Start"/>
        <ButtonV1 />
        <ButtonV1 />
        <ButtonV1 />
      </div>
    )
  }
}