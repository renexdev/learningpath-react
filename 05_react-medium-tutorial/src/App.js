import React, { Component, createElement } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import ButtonAppV1 from './ebook/ch08_defaultProps/ButtonAppV1'
import ButtonAppV2 from './ebook/ch08_defaultProps/ButtonAppV2'


const node  = document.querySelector('.main');
//const jsxElement =  <GeneralForm data-url="http://webapplog.com" />
//const jsxElement =  <AccountForm />
//const jsxElement =  <ButtonAppV1 />
const jsxElement =  <ButtonAppV2 />


render(jsxElement, node)