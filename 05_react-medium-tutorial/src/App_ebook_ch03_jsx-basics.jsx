import React, { Component, createElement } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'

const node  = document.querySelector('.main');

const helloWorldReactElement = <h1>Hello world!</h1>

class HelloWorld extends Component {
  render() {
    return <div>
      {helloWorldReactElement}
      {helloWorldReactElement}
    </div>
  }
}

class ProfileLink extends Component {
	render() {
		return <a href={this.props.url}
		title={this.props.label}
		target="_blank">Profile
		</a>
	}
}

const jsxElement = <ProfileLink url='/users/renex' label='Profile renex'/>

//const jsxElement = <HelloWorld/>

render(jsxElement, node)