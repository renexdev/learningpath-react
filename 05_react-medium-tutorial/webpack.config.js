var webpack = require('webpack');
var path = require('path');

module.exports = {
  entry: [
    './src/App'
  ],
  module: {
loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel',
        exclude: /node_modules/,
        query: {
          cacheDirectory: true,
          presets: ['react', 'es2015']
        }
      }
    ]
 },
 // Allows for you to not have to specify file extensions...Just add more file extensions to the array if needed
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  // Output to dist folder as bundle.js
  output: {
    path: path.join(__dirname, '/dist'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  // Enable hot reload while webpack dev server is in use
  devServer: {
    contentBase: './dist',
    hot: true
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
};
